module lambdadb

go 1.15

require (
	github.com/JensRantil/go-csv v0.0.0-20200923162218-7ffda755f61b
	github.com/Workiva/go-datastructures v1.0.52
	github.com/cheggaaa/pb v1.0.29
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-spatial/geom v0.0.0-20200810200216-9bf4204b30e9
	github.com/golang/geo v0.0.0-20200730024412-e86565bf3f35
	github.com/klauspost/compress v1.11.7 // indirect
	github.com/klauspost/pgzip v1.2.5
	github.com/kr/pretty v0.1.0 // indirect
	github.com/lib/pq v1.8.0
	github.com/pkg/errors v0.9.1
	golang.org/x/sys v0.0.0-20210309074719-68d13333faf2 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
