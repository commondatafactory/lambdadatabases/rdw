package main

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	_ "github.com/lib/pq"
)

// lastTableComment is used to keep track
// of update on database source table.
var lastTableComment string
var sourceTable = "bovag.rdw_bag_matched"

// ConnectStr create string to connect to database
func ConnectStr() string {
	otherParams := "connect_timeout=5"
	return fmt.Sprintf(
		"user=%s dbname=%s password='%s' host=%s port=%s sslmode=%s  %s",
		SETTINGS.Get("PGUSER"),
		SETTINGS.Get("PGDATABASE"),
		SETTINGS.Get("PGPASSWORD"),
		SETTINGS.Get("PGHOST"),
		SETTINGS.Get("PGPORT"),
		SETTINGS.Get("PGSSLMODE"),
		otherParams,
	)
}

func dbConnect(connStr string) (*sql.DB, error) {
	//connStr := connectStr()
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return db, err
	}

	err = db.Ping()
	return db, err
}

type ItemInSQL struct {
	Id                    sql.NullString
	Vid                   sql.NullString
	Pid                   sql.NullString
	Sid                   sql.NullString
	Lid                   sql.NullString
	Numid                 sql.NullString
	Postcode              sql.NullString
	Huisnummer            sql.NullString
	Huisletter            sql.NullString
	Huisnummertoevoeging  sql.NullString
	RdwToevoeging         sql.NullString
	Volgnummer            sql.NullString
	Gevelnaam             sql.NullString
	NaamBedrijf           sql.NullString
	Straat                sql.NullString
	Plaats                sql.NullString
	ApiBedrijfErkenningen sql.NullString
	Oppervlakte           sql.NullString
	Geometry              sql.NullString
	MatchScore            sql.NullString
}

type Comment struct {
	Comment sql.NullString
}

/*
 * source database table should
 * have comment if comment is
 * changed we reload data.
 */
func tableComment() (string, error) {
	db, err := dbConnect(ConnectStr())

	if err != nil {
		log.Println(err)
		return "", err
	}


	defer db.Close()

	query := fmt.Sprintf(`
	select obj_description('%s'::regclass);
	`, sourceTable)

	rows, err := db.Query(query)

	if err != nil {
		log.Println(err)
		return "", err
	}

	c := Comment{}

	for rows.Next() {
		if err := rows.Scan(&(c).Comment); err != nil {
			// Check for a scan error.
			// Query rows will be closed with defer.
			log.Println(err)
			return "", err
		}
	}
	comment := convertSqlNullString(c.Comment)
	// fmt.Printf("%s %s \n", sourceTable, comment)
	return comment, nil
}

// Every x seconds check database for changed comment.
// if changed replace data in ITEMS
//
// attempt reload if data is missing
// crash if S2CELSS are empty.
func CheckForChange() {

	uptimeTicker := time.NewTicker(35 * time.Second)
	validateTicker := time.NewTicker(3600 * time.Second)
	failTicker := time.NewTicker(3800 * time.Second)

	for {
		select {
		case <-uptimeTicker.C:
			newComment, err := tableComment()
			if err == nil && newComment != lastTableComment {
				if newComment != "" {
					reset()
					fillFromDB(itemChan)
				}
			}
		case <-validateTicker.C:
			if len(ITEMS) < 100 {
				reset()
				fillFromDB(itemChan)
			}
		case <-failTicker.C:
			if len(S2CELLS) < 100 {
				log.Fatal("missing geo data, force crash")
			}
		}
	}
}

func fillFromDB(items ItemsChannel) {
	// update last know comment from source table
	// so we can detect changes.
	comment, err := tableComment()

	if err != nil {
		log.Fatal(err)
	}

	lastTableComment = comment

	db, err := dbConnect(ConnectStr())
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	query := fmt.Sprintf(`
	SELECT
	    id,
	    vid,
	    pid,
	    sid,
	    lid,
	    numid,
	    postcode,
	    huisnummer,
	    huisletter,
	    huisnummertoevoeging,
	    rdw_toevoeging,
	    volgnummer,
	    gevelnaam,
	    naam_bedrijf,
	    straat,
	    plaats,
	    api_bedrijf_erkenningen,
	    oppervlakte,
	    st_astext(st_transform(geometry, 4326)) as geometry,
	    match_score
	FROM %s;
 	`, sourceTable)
	rows, err := db.Query(query)

	if err != nil {
		log.Fatal(err)
	}

	itemsArray := ItemsIn{}
	in := ItemInSQL{}

	rowCounter := 0

	for rows.Next() {
		if err := rows.Scan(
			&(in).Id,
			&(in).Vid,
			&(in).Pid,
			&(in).Sid,
			&(in).Lid,
			&(in).Numid,
			&(in).Postcode,
			&(in).Huisnummer,
			&(in).Huisletter,
			&(in).Huisnummertoevoeging,
			&(in).RdwToevoeging,
			&(in).Volgnummer,
			&(in).Gevelnaam,
			&(in).NaamBedrijf,
			&(in).Straat,
			&(in).Plaats,
			&(in).ApiBedrijfErkenningen,
			&(in).Oppervlakte,
			&(in).Geometry,
			&(in).MatchScore,
		); err != nil {
			// Check for a scan error.
			// Query rows will be closed with defer.
			log.Fatal(err)
		}

		itemIn := &ItemIn{
			convertSqlNullString(in.Id),
			convertSqlNullString(in.Vid),
			convertSqlNullString(in.Pid),
			convertSqlNullString(in.Sid),
			convertSqlNullString(in.Lid),
			convertSqlNullString(in.Numid),
			convertSqlNullString(in.Postcode),
			convertSqlNullString(in.Huisnummer),
			convertSqlNullString(in.Huisletter),
			convertSqlNullString(in.Huisnummertoevoeging),
			convertSqlNullString(in.RdwToevoeging),
			convertSqlNullString(in.Volgnummer),
			convertSqlNullString(in.Gevelnaam),
			convertSqlNullString(in.NaamBedrijf),
			convertSqlNullString(in.Straat),
			convertSqlNullString(in.Plaats),
			convertSqlNullString(in.ApiBedrijfErkenningen),
			convertSqlNullString(in.Oppervlakte),
			convertSqlNullString(in.Geometry),
			convertSqlNullString(in.MatchScore),
		}

		if len(itemsArray) > 1000 {
			itemChan <- itemsArray
			itemsArray = ItemsIn{}
		}

		itemsArray = append(itemsArray, itemIn)
		rowCounter += 1
	}

	// add left over items
	itemChan <- itemsArray

	// If the database is being written to ensure to check for Close
	// errors that may be returned from the driver. The query may
	// encounter an auto-commit error and be forced to rollback changes.
	rerr := rows.Close()

	if rerr != nil {
		log.Fatal(err)
	}
	// Rows.Err will report the last error encountered by Rows.Scan.
	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}

	lock.Lock()
	defer lock.Unlock()

	S2CELLS.Sort()

	log.Printf("added %d rows from database", rowCounter)
}

func convertSqlNullString(v sql.NullString) string {
	if v.Valid {
		return v.String
	} else {
		return ""
	}
}

/*
func convertSqlNullInt(v sql.NullInt64) int64 {
	var err error
	var output []byte

	if v.Valid {
		output, err = json.Marshal(v.Int64)
	} else {
		output, err = json.Marshal(nil)
		return int64(0)
	}

	if err != nil {
		panic(err)
	}

	bla, err := strconv.ParseInt(string(output), 10, 64)

	if err != nil {
		panic(err)
	}

	return bla

}

func convertSqlNullFloat(v sql.NullFloat64) float64 {
	var err error
	var output []byte

	if v.Valid {
		output, err = json.Marshal(v.Float64)
	} else {
		output, err = json.Marshal(nil)
		return float64(0)
	}

	if err != nil {
		panic(err)
	}

	bla, err := strconv.ParseFloat(string(output), 64)

	if err != nil {
		panic(err)
	}

	return bla

}
*/
