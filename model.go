/*
	model.go define the 'items' to store.
	All columns with getters and setters are defined here.

	ItemIn, represent rows from the Input data
	Item, the compact item stored in memmory
	ItemOut, defines how and which fields are exported out
	of the API. It is possible to ignore input columns

	Repeated values are stored in maps with int numbers
	as keys.  Optionally bitarrays are created for reapeated
	column values to do fast bit-wise filtering.

	A S2 geo index in created for lat, lon values.

	Unique values are stored as-is.

	The generated codes leaves room to create custom
	index functions yourself to create an API with an
	< 1 ms response time for your specific needs.

	This codebase solves: I need to have an API on this
	tabular dataset fast!
*/

package main

import (
	"encoding/json"
	"errors"
	"sort"
	"strconv"
	"strings"

	"github.com/Workiva/go-datastructures/bitarray"
)

type registerGroupByFunc map[string]func(*Item) string
type registerGettersMap map[string]func(*Item) string
type registerReduce map[string]func(Items) map[string]string

type registerBitArray map[string]func(s string) (bitarray.BitArray, error)
type fieldBitarrayMap map[uint32]bitarray.BitArray

type ItemIn struct {
	Id                    string `json:"id"`
	Vid                   string `json:"vid"`
	Pid                   string `json:"pid"`
	Sid                   string `json:"sid"`
	Lid                   string `json:"lid"`
	Numid                 string `json:"numid"`
	Postcode              string `json:"postcode"`
	Huisnummer            string `json:"huisnummer"`
	Huisletter            string `json:"huisletter"`
	Huisnummertoevoeging  string `json:"huisnummertoevoeging"`
	RdwToevoeging         string `json:"rdw_toevoeging"`
	Volgnummer            string `json:"volgnummer"`
	Gevelnaam             string `json:"gevelnaam"`
	NaamBedrijf           string `json:"naam_bedrijf"`
	Straat                string `json:"straat"`
	Plaats                string `json:"plaats"`
	ApiBedrijfErkenningen string `json:"api_bedrijf_erkenningen"`
	Oppervlakte           string `json:"oppervlakte"`
	Geometry              string `json:"geometry"`
	MatchScore            string `json:"match_score"`
}

type ItemOut struct {
	Id                   string `json:"id"`
	Vid                  string `json:"vid"`
	Pid                  string `json:"pid"`
	Sid                  string `json:"sid"`
	Lid                  string `json:"lid"`
	Numid                string `json:"numid"`
	Postcode             string `json:"postcode"`
	Huisnummer           string `json:"huisnummer"`
	Huisletter           string `json:"huisletter"`
	Huisnummertoevoeging string `json:"huisnummertoevoeging"`
	RdwToevoeging        string `json:"rdw_toevoeging"`
	Volgnummer           string `json:"volgnummer"`
	Gevelnaam            string `json:"gevelnaam"`
	NaamBedrijf          string `json:"naam_bedrijf"`
	Straat               string `json:"straat"`
	Plaats               string `json:"plaats"`
	Oppervlakte          string `json:"oppervlakte"`
	Geometry             string `json:"geometry"`
	MatchScore           string `json:"match_score"`
}

type Item struct {
	Label                int // internal index in ITEMS
	Id                   string
	Vid                  string
	Pid                  string
	Sid                  string
	Lid                  string
	Numid                string
	Postcode             string
	Huisnummer           string
	Huisletter           string
	Huisnummertoevoeging string
	RdwToevoeging        string
	Volgnummer           string
	Gevelnaam            string
	NaamBedrijf          string
	Straat               string
	Plaats               string
	Oppervlakte          string
	Geometry             string
	MatchScore           string
}

func (i Item) MarshalJSON() ([]byte, error) {
	return json.Marshal(i.Serialize())
}

// Shrink create smaller Item using uint32
func (i ItemIn) Shrink(label int) Item {

	return Item{

		label,

		i.Id,
		i.Vid,
		i.Pid,
		i.Sid,
		i.Lid,
		i.Numid,
		i.Postcode,
		i.Huisnummer,
		i.Huisletter,
		i.Huisnummertoevoeging,
		i.RdwToevoeging,
		i.Volgnummer,
		i.Gevelnaam,
		i.NaamBedrijf,
		i.Straat,
		i.Plaats,
		i.Oppervlakte,
		i.Geometry,
		i.MatchScore,
	}
}

// Store selected columns in seperate map[columnvalue]bitarray
// for fast item selection
func (i *Item) StoreBitArrayColumns() {

}

func (i Item) Serialize() ItemOut {
	return ItemOut{

		i.Id,
		i.Vid,
		i.Pid,
		i.Sid,
		i.Lid,
		i.Numid,
		i.Postcode,
		i.Huisnummer,
		i.Huisletter,
		i.Huisnummertoevoeging,
		i.RdwToevoeging,
		i.Volgnummer,
		i.Gevelnaam,
		i.NaamBedrijf,
		i.Straat,
		i.Plaats,
		i.Oppervlakte,
		i.Geometry,
		i.MatchScore,
	}
}

func (i ItemIn) Columns() []string {
	return []string{

		"id",
		"vid",
		"pid",
		"sid",
		"lid",
		"numid",
		"postcode",
		"huisnummer",
		"huisletter",
		"huisnummertoevoeging",
		"rdw_toevoeging",
		"volgnummer",
		"gevelnaam",
		"naam_bedrijf",
		"straat",
		"plaats",
		"api_bedrijf_erkenningen",
		"oppervlakte",
		"geometry",
		"match_score",
	}
}

func (i ItemOut) Columns() []string {
	return []string{

		"id",
		"vid",
		"pid",
		"sid",
		"lid",
		"numid",
		"postcode",
		"huisnummer",
		"huisletter",
		"huisnummertoevoeging",
		"rdw_toevoeging",
		"volgnummer",
		"gevelnaam",
		"naam_bedrijf",
		"straat",
		"plaats",
		"oppervlakte",
		"geometry",
		"match_score",
	}
}

func (i Item) Row() []string {

	return []string{

		i.Id,
		i.Vid,
		i.Pid,
		i.Sid,
		i.Lid,
		i.Numid,
		i.Postcode,
		i.Huisnummer,
		i.Huisletter,
		i.Huisnummertoevoeging,
		i.RdwToevoeging,
		i.Volgnummer,
		i.Gevelnaam,
		i.NaamBedrijf,
		i.Straat,
		i.Plaats,
		i.Oppervlakte,
		i.Geometry,
		i.MatchScore,
	}
}

func (i Item) GetIndex() string {
	return GettersId(&i)
}

func (i Item) GetGeometry() string {
	return GettersGeometry(&i)
}

// contain filter Id
func FilterIdContains(i *Item, s string) bool {
	return strings.Contains(i.Id, s)
}

// startswith filter Id
func FilterIdStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Id, s)
}

// match filters Id
func FilterIdMatch(i *Item, s string) bool {
	return i.Id == s
}

// getter Id
func GettersId(i *Item) string {
	return i.Id
}

// contain filter Vid
func FilterVidContains(i *Item, s string) bool {
	return strings.Contains(i.Vid, s)
}

// startswith filter Vid
func FilterVidStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Vid, s)
}

// match filters Vid
func FilterVidMatch(i *Item, s string) bool {
	return i.Vid == s
}

// getter Vid
func GettersVid(i *Item) string {
	return i.Vid
}

// contain filter Pid
func FilterPidContains(i *Item, s string) bool {
	return strings.Contains(i.Pid, s)
}

// startswith filter Pid
func FilterPidStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Pid, s)
}

// match filters Pid
func FilterPidMatch(i *Item, s string) bool {
	return i.Pid == s
}

// getter Pid
func GettersPid(i *Item) string {
	return i.Pid
}

// contain filter Sid
func FilterSidContains(i *Item, s string) bool {
	return strings.Contains(i.Sid, s)
}

// startswith filter Sid
func FilterSidStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Sid, s)
}

// match filters Sid
func FilterSidMatch(i *Item, s string) bool {
	return i.Sid == s
}

// getter Sid
func GettersSid(i *Item) string {
	return i.Sid
}

// contain filter Lid
func FilterLidContains(i *Item, s string) bool {
	return strings.Contains(i.Lid, s)
}

// startswith filter Lid
func FilterLidStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Lid, s)
}

// match filters Lid
func FilterLidMatch(i *Item, s string) bool {
	return i.Lid == s
}

// getter Lid
func GettersLid(i *Item) string {
	return i.Lid
}

// contain filter Numid
func FilterNumidContains(i *Item, s string) bool {
	return strings.Contains(i.Numid, s)
}

// startswith filter Numid
func FilterNumidStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Numid, s)
}

// match filters Numid
func FilterNumidMatch(i *Item, s string) bool {
	return i.Numid == s
}

// getter Numid
func GettersNumid(i *Item) string {
	return i.Numid
}

// contain filter Postcode
func FilterPostcodeContains(i *Item, s string) bool {
	return strings.Contains(i.Postcode, s)
}

// startswith filter Postcode
func FilterPostcodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Postcode, s)
}

// match filters Postcode
func FilterPostcodeMatch(i *Item, s string) bool {
	return i.Postcode == s
}

// getter Postcode
func GettersPostcode(i *Item) string {
	return i.Postcode
}

// contain filter Huisnummer
func FilterHuisnummerContains(i *Item, s string) bool {
	return strings.Contains(i.Huisnummer, s)
}

// startswith filter Huisnummer
func FilterHuisnummerStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Huisnummer, s)
}

// match filters Huisnummer
func FilterHuisnummerMatch(i *Item, s string) bool {
	return i.Huisnummer == s
}

// getter Huisnummer
func GettersHuisnummer(i *Item) string {
	return i.Huisnummer
}

// contain filter Huisletter
func FilterHuisletterContains(i *Item, s string) bool {
	return strings.Contains(i.Huisletter, s)
}

// startswith filter Huisletter
func FilterHuisletterStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Huisletter, s)
}

// match filters Huisletter
func FilterHuisletterMatch(i *Item, s string) bool {
	return i.Huisletter == s
}

// getter Huisletter
func GettersHuisletter(i *Item) string {
	return i.Huisletter
}

// contain filter Huisnummertoevoeging
func FilterHuisnummertoevoegingContains(i *Item, s string) bool {
	return strings.Contains(i.Huisnummertoevoeging, s)
}

// startswith filter Huisnummertoevoeging
func FilterHuisnummertoevoegingStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Huisnummertoevoeging, s)
}

// match filters Huisnummertoevoeging
func FilterHuisnummertoevoegingMatch(i *Item, s string) bool {
	return i.Huisnummertoevoeging == s
}

// getter Huisnummertoevoeging
func GettersHuisnummertoevoeging(i *Item) string {
	return i.Huisnummertoevoeging
}

// contain filter RdwToevoeging
func FilterRdwToevoegingContains(i *Item, s string) bool {
	return strings.Contains(i.RdwToevoeging, s)
}

// startswith filter RdwToevoeging
func FilterRdwToevoegingStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.RdwToevoeging, s)
}

// match filters RdwToevoeging
func FilterRdwToevoegingMatch(i *Item, s string) bool {
	return i.RdwToevoeging == s
}

// getter RdwToevoeging
func GettersRdwToevoeging(i *Item) string {
	return i.RdwToevoeging
}

// contain filter Volgnummer
func FilterVolgnummerContains(i *Item, s string) bool {
	return strings.Contains(i.Volgnummer, s)
}

// startswith filter Volgnummer
func FilterVolgnummerStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Volgnummer, s)
}

// match filters Volgnummer
func FilterVolgnummerMatch(i *Item, s string) bool {
	return i.Volgnummer == s
}

// getter Volgnummer
func GettersVolgnummer(i *Item) string {
	return i.Volgnummer
}

// contain filter Gevelnaam
func FilterGevelnaamContains(i *Item, s string) bool {
	return strings.Contains(i.Gevelnaam, s)
}

// startswith filter Gevelnaam
func FilterGevelnaamStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Gevelnaam, s)
}

// match filters Gevelnaam
func FilterGevelnaamMatch(i *Item, s string) bool {
	return i.Gevelnaam == s
}

// getter Gevelnaam
func GettersGevelnaam(i *Item) string {
	return i.Gevelnaam
}

// contain filter NaamBedrijf
func FilterNaamBedrijfContains(i *Item, s string) bool {
	return strings.Contains(i.NaamBedrijf, s)
}

// startswith filter NaamBedrijf
func FilterNaamBedrijfStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.NaamBedrijf, s)
}

// match filters NaamBedrijf
func FilterNaamBedrijfMatch(i *Item, s string) bool {
	return i.NaamBedrijf == s
}

// getter NaamBedrijf
func GettersNaamBedrijf(i *Item) string {
	return i.NaamBedrijf
}

// contain filter Straat
func FilterStraatContains(i *Item, s string) bool {
	return strings.Contains(i.Straat, s)
}

// startswith filter Straat
func FilterStraatStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Straat, s)
}

// match filters Straat
func FilterStraatMatch(i *Item, s string) bool {
	return i.Straat == s
}

// getter Straat
func GettersStraat(i *Item) string {
	return i.Straat
}

// contain filter Plaats
func FilterPlaatsContains(i *Item, s string) bool {
	return strings.Contains(i.Plaats, s)
}

// startswith filter Plaats
func FilterPlaatsStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Plaats, s)
}

// match filters Plaats
func FilterPlaatsMatch(i *Item, s string) bool {
	return i.Plaats == s
}

// getter Plaats
func GettersPlaats(i *Item) string {
	return i.Plaats
}

// contain filter Oppervlakte
func FilterOppervlakteContains(i *Item, s string) bool {
	return strings.Contains(i.Oppervlakte, s)
}

// startswith filter Oppervlakte
func FilterOppervlakteStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Oppervlakte, s)
}

// match filters Oppervlakte
func FilterOppervlakteMatch(i *Item, s string) bool {
	return i.Oppervlakte == s
}

// getter Oppervlakte
func GettersOppervlakte(i *Item) string {
	return i.Oppervlakte
}

// contain filter Geometry
func FilterGeometryContains(i *Item, s string) bool {
	return strings.Contains(i.Geometry, s)
}

// startswith filter Geometry
func FilterGeometryStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Geometry, s)
}

// match filters Geometry
func FilterGeometryMatch(i *Item, s string) bool {
	return i.Geometry == s
}

// getter Geometry
func GettersGeometry(i *Item) string {
	return i.Geometry
}

// contain filter MatchScore
func FilterMatchScoreContains(i *Item, s string) bool {
	return strings.Contains(i.MatchScore, s)
}

// startswith filter MatchScore
func FilterMatchScoreStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.MatchScore, s)
}

// match filters MatchScore
func FilterMatchScoreMatch(i *Item, s string) bool {
	return i.MatchScore == s
}

// getter MatchScore
func GettersMatchScore(i *Item) string {
	return i.MatchScore
}

/*
// contain filters
func FilterEkeyContains(i *Item, s string) bool {
	return strings.Contains(i.Ekey, s)
}


// startswith filters
func FilterEkeyStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Ekey, s)
}


// match filters
func FilterEkeyMatch(i *Item, s string) bool {
	return i.Ekey == s
}

// getters
func GettersEkey(i *Item) string {
	return i.Ekey
}
*/

// reduce functions
func reduceCount(items Items) map[string]string {
	result := make(map[string]string)
	result["count"] = strconv.Itoa(len(items))
	return result
}

type GroupedOperations struct {
	Funcs     registerFuncType
	GroupBy   registerGroupByFunc
	Getters   registerGettersMap
	Reduce    registerReduce
	BitArrays registerBitArray
}

var Operations GroupedOperations

var RegisterFuncMap registerFuncType
var RegisterGroupBy registerGroupByFunc
var RegisterGetters registerGettersMap
var RegisterReduce registerReduce
var RegisterBitArray registerBitArray

// ValidateRegsiters validate exposed columns do match filter names
func validateRegisters() error {
	var i = ItemOut{}
	var filters = []string{"match", "contains", "startswith"}
	for _, c := range i.Columns() {
		for _, f := range filters {
			if _, ok := RegisterFuncMap[f+"-"+c]; !ok {
				return errors.New(c + " is missing in RegisterMap")
			}
		}
	}
	return nil
}

// contain filter NaamBedrijf
func SearchFilter(i *Item, s string) bool {
	return (strings.Contains(i.NaamBedrijf, s) ||
		strings.Contains(i.Postcode, s) ||
		strings.Contains(i.Numid, s) ||
		strings.Contains(i.Gevelnaam, s) ||
		strings.Contains(i.Straat, s) ||
		strings.Contains(i.Plaats, s))
}

func init() {

	RegisterFuncMap = make(registerFuncType)
	RegisterGroupBy = make(registerGroupByFunc)
	RegisterGetters = make(registerGettersMap)
	RegisterReduce = make(registerReduce)

	// register search filter.
	//RegisterFuncMap["search"] = 'EDITYOURSELF'
	RegisterFuncMap["search"] = SearchFilter

	//RegisterFuncMap["value"] = 'EDITYOURSELF'
	RegisterGetters["value"] = GettersNaamBedrijf

	// register filters

	//register filters for Id
	RegisterFuncMap["match-id"] = FilterIdMatch
	RegisterFuncMap["contains-id"] = FilterIdContains
	RegisterFuncMap["startswith-id"] = FilterIdStartsWith
	RegisterGetters["id"] = GettersId
	RegisterGroupBy["id"] = GettersId

	//register filters for Vid
	RegisterFuncMap["match-vid"] = FilterVidMatch
	RegisterFuncMap["contains-vid"] = FilterVidContains
	RegisterFuncMap["startswith-vid"] = FilterVidStartsWith
	RegisterGetters["vid"] = GettersVid
	RegisterGroupBy["vid"] = GettersVid

	//register filters for Pid
	RegisterFuncMap["match-pid"] = FilterPidMatch
	RegisterFuncMap["contains-pid"] = FilterPidContains
	RegisterFuncMap["startswith-pid"] = FilterPidStartsWith
	RegisterGetters["pid"] = GettersPid
	RegisterGroupBy["pid"] = GettersPid

	//register filters for Sid
	RegisterFuncMap["match-sid"] = FilterSidMatch
	RegisterFuncMap["contains-sid"] = FilterSidContains
	RegisterFuncMap["startswith-sid"] = FilterSidStartsWith
	RegisterGetters["sid"] = GettersSid
	RegisterGroupBy["sid"] = GettersSid

	//register filters for Lid
	RegisterFuncMap["match-lid"] = FilterLidMatch
	RegisterFuncMap["contains-lid"] = FilterLidContains
	RegisterFuncMap["startswith-lid"] = FilterLidStartsWith
	RegisterGetters["lid"] = GettersLid
	RegisterGroupBy["lid"] = GettersLid

	//register filters for Numid
	RegisterFuncMap["match-numid"] = FilterNumidMatch
	RegisterFuncMap["contains-numid"] = FilterNumidContains
	RegisterFuncMap["startswith-numid"] = FilterNumidStartsWith
	RegisterGetters["numid"] = GettersNumid
	RegisterGroupBy["numid"] = GettersNumid

	//register filters for Postcode
	RegisterFuncMap["match-postcode"] = FilterPostcodeMatch
	RegisterFuncMap["contains-postcode"] = FilterPostcodeContains
	RegisterFuncMap["startswith-postcode"] = FilterPostcodeStartsWith
	RegisterGetters["postcode"] = GettersPostcode
	RegisterGroupBy["postcode"] = GettersPostcode

	//register filters for Huisnummer
	RegisterFuncMap["match-huisnummer"] = FilterHuisnummerMatch
	RegisterFuncMap["contains-huisnummer"] = FilterHuisnummerContains
	RegisterFuncMap["startswith-huisnummer"] = FilterHuisnummerStartsWith
	RegisterGetters["huisnummer"] = GettersHuisnummer
	RegisterGroupBy["huisnummer"] = GettersHuisnummer

	//register filters for Huisletter
	RegisterFuncMap["match-huisletter"] = FilterHuisletterMatch
	RegisterFuncMap["contains-huisletter"] = FilterHuisletterContains
	RegisterFuncMap["startswith-huisletter"] = FilterHuisletterStartsWith
	RegisterGetters["huisletter"] = GettersHuisletter
	RegisterGroupBy["huisletter"] = GettersHuisletter

	//register filters for Huisnummertoevoeging
	RegisterFuncMap["match-huisnummertoevoeging"] = FilterHuisnummertoevoegingMatch
	RegisterFuncMap["contains-huisnummertoevoeging"] = FilterHuisnummertoevoegingContains
	RegisterFuncMap["startswith-huisnummertoevoeging"] = FilterHuisnummertoevoegingStartsWith
	RegisterGetters["huisnummertoevoeging"] = GettersHuisnummertoevoeging
	RegisterGroupBy["huisnummertoevoeging"] = GettersHuisnummertoevoeging

	//register filters for RdwToevoeging
	RegisterFuncMap["match-rdw_toevoeging"] = FilterRdwToevoegingMatch
	RegisterFuncMap["contains-rdw_toevoeging"] = FilterRdwToevoegingContains
	RegisterFuncMap["startswith-rdw_toevoeging"] = FilterRdwToevoegingStartsWith
	RegisterGetters["rdw_toevoeging"] = GettersRdwToevoeging
	RegisterGroupBy["rdw_toevoeging"] = GettersRdwToevoeging

	//register filters for Volgnummer
	RegisterFuncMap["match-volgnummer"] = FilterVolgnummerMatch
	RegisterFuncMap["contains-volgnummer"] = FilterVolgnummerContains
	RegisterFuncMap["startswith-volgnummer"] = FilterVolgnummerStartsWith
	RegisterGetters["volgnummer"] = GettersVolgnummer
	RegisterGroupBy["volgnummer"] = GettersVolgnummer

	//register filters for Gevelnaam
	RegisterFuncMap["match-gevelnaam"] = FilterGevelnaamMatch
	RegisterFuncMap["contains-gevelnaam"] = FilterGevelnaamContains
	RegisterFuncMap["startswith-gevelnaam"] = FilterGevelnaamStartsWith
	RegisterGetters["gevelnaam"] = GettersGevelnaam
	RegisterGroupBy["gevelnaam"] = GettersGevelnaam

	//register filters for NaamBedrijf
	RegisterFuncMap["match-naam_bedrijf"] = FilterNaamBedrijfMatch
	RegisterFuncMap["contains-naam_bedrijf"] = FilterNaamBedrijfContains
	RegisterFuncMap["startswith-naam_bedrijf"] = FilterNaamBedrijfStartsWith
	RegisterGetters["naam_bedrijf"] = GettersNaamBedrijf
	RegisterGroupBy["naam_bedrijf"] = GettersNaamBedrijf

	//register filters for Straat
	RegisterFuncMap["match-straat"] = FilterStraatMatch
	RegisterFuncMap["contains-straat"] = FilterStraatContains
	RegisterFuncMap["startswith-straat"] = FilterStraatStartsWith
	RegisterGetters["straat"] = GettersStraat
	RegisterGroupBy["straat"] = GettersStraat

	//register filters for Plaats
	RegisterFuncMap["match-plaats"] = FilterPlaatsMatch
	RegisterFuncMap["contains-plaats"] = FilterPlaatsContains
	RegisterFuncMap["startswith-plaats"] = FilterPlaatsStartsWith
	RegisterGetters["plaats"] = GettersPlaats
	RegisterGroupBy["plaats"] = GettersPlaats

	//register filters for Oppervlakte
	RegisterFuncMap["match-oppervlakte"] = FilterOppervlakteMatch
	RegisterFuncMap["contains-oppervlakte"] = FilterOppervlakteContains
	RegisterFuncMap["startswith-oppervlakte"] = FilterOppervlakteStartsWith
	RegisterGetters["oppervlakte"] = GettersOppervlakte
	RegisterGroupBy["oppervlakte"] = GettersOppervlakte

	//register filters for Geometry
	RegisterFuncMap["match-geometry"] = FilterGeometryMatch
	RegisterFuncMap["contains-geometry"] = FilterGeometryContains
	RegisterFuncMap["startswith-geometry"] = FilterGeometryStartsWith
	RegisterGetters["geometry"] = GettersGeometry
	RegisterGroupBy["geometry"] = GettersGeometry

	//register filters for MatchScore
	RegisterFuncMap["match-match_score"] = FilterMatchScoreMatch
	RegisterFuncMap["contains-match_score"] = FilterMatchScoreContains
	RegisterFuncMap["startswith-match_score"] = FilterMatchScoreStartsWith
	RegisterGetters["match_score"] = GettersMatchScore
	RegisterGroupBy["match_score"] = GettersMatchScore

	validateRegisters()

	/*
		RegisterFuncMap["match-ekey"] = FilterEkeyMatch
		RegisterFuncMap["contains-ekey"] = FilterEkeyContains
		// register startswith filters
		RegisterFuncMap["startswith-ekey"] = FilterEkeyStartsWith
		// register getters
		RegisterGetters["ekey"] = GettersEkey
		// register groupby
		RegisterGroupBy["ekey"] = GettersEkey

	*/

	// register reduce functions
	RegisterReduce["count"] = reduceCount
}

type sortLookup map[string]func(int, int) bool

func createSort(items Items) sortLookup {

	sortFuncs := sortLookup{

		"id":  func(i, j int) bool { return items[i].Id < items[j].Id },
		"-id": func(i, j int) bool { return items[i].Id > items[j].Id },

		"vid":  func(i, j int) bool { return items[i].Vid < items[j].Vid },
		"-vid": func(i, j int) bool { return items[i].Vid > items[j].Vid },

		"pid":  func(i, j int) bool { return items[i].Pid < items[j].Pid },
		"-pid": func(i, j int) bool { return items[i].Pid > items[j].Pid },

		"sid":  func(i, j int) bool { return items[i].Sid < items[j].Sid },
		"-sid": func(i, j int) bool { return items[i].Sid > items[j].Sid },

		"lid":  func(i, j int) bool { return items[i].Lid < items[j].Lid },
		"-lid": func(i, j int) bool { return items[i].Lid > items[j].Lid },

		"numid":  func(i, j int) bool { return items[i].Numid < items[j].Numid },
		"-numid": func(i, j int) bool { return items[i].Numid > items[j].Numid },

		"postcode":  func(i, j int) bool { return items[i].Postcode < items[j].Postcode },
		"-postcode": func(i, j int) bool { return items[i].Postcode > items[j].Postcode },

		"huisnummer":  func(i, j int) bool { return items[i].Huisnummer < items[j].Huisnummer },
		"-huisnummer": func(i, j int) bool { return items[i].Huisnummer > items[j].Huisnummer },

		"huisletter":  func(i, j int) bool { return items[i].Huisletter < items[j].Huisletter },
		"-huisletter": func(i, j int) bool { return items[i].Huisletter > items[j].Huisletter },

		"huisnummertoevoeging":  func(i, j int) bool { return items[i].Huisnummertoevoeging < items[j].Huisnummertoevoeging },
		"-huisnummertoevoeging": func(i, j int) bool { return items[i].Huisnummertoevoeging > items[j].Huisnummertoevoeging },

		"rdw_toevoeging":  func(i, j int) bool { return items[i].RdwToevoeging < items[j].RdwToevoeging },
		"-rdw_toevoeging": func(i, j int) bool { return items[i].RdwToevoeging > items[j].RdwToevoeging },

		"volgnummer":  func(i, j int) bool { return items[i].Volgnummer < items[j].Volgnummer },
		"-volgnummer": func(i, j int) bool { return items[i].Volgnummer > items[j].Volgnummer },

		"gevelnaam":  func(i, j int) bool { return items[i].Gevelnaam < items[j].Gevelnaam },
		"-gevelnaam": func(i, j int) bool { return items[i].Gevelnaam > items[j].Gevelnaam },

		"naam_bedrijf":  func(i, j int) bool { return items[i].NaamBedrijf < items[j].NaamBedrijf },
		"-naam_bedrijf": func(i, j int) bool { return items[i].NaamBedrijf > items[j].NaamBedrijf },

		"straat":  func(i, j int) bool { return items[i].Straat < items[j].Straat },
		"-straat": func(i, j int) bool { return items[i].Straat > items[j].Straat },

		"plaats":  func(i, j int) bool { return items[i].Plaats < items[j].Plaats },
		"-plaats": func(i, j int) bool { return items[i].Plaats > items[j].Plaats },

		"oppervlakte":  func(i, j int) bool { return items[i].Oppervlakte < items[j].Oppervlakte },
		"-oppervlakte": func(i, j int) bool { return items[i].Oppervlakte > items[j].Oppervlakte },

		"geometry":  func(i, j int) bool { return items[i].Geometry < items[j].Geometry },
		"-geometry": func(i, j int) bool { return items[i].Geometry > items[j].Geometry },

		"match_score":  func(i, j int) bool { return items[i].MatchScore < items[j].MatchScore },
		"-match_score": func(i, j int) bool { return items[i].MatchScore > items[j].MatchScore },
	}
	return sortFuncs
}

func sortBy(items Items, sortingL []string) (Items, []string) {
	sortFuncs := createSort(items)

	for _, sortFuncName := range sortingL {
		sortFunc, ok := sortFuncs[sortFuncName]
		if ok {
			sort.Slice(items, sortFunc)
		}
	}

	// TODO must be nicer way
	keys := []string{}
	for key := range sortFuncs {
		keys = append(keys, key)
	}

	return items, keys
}
